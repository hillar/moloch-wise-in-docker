#!/bin/sh

[ -z "${CONFIGFILE}" ] && CONFIGFILE="wiseService.ini"
CONFIGPATH="../etc"
CONFIG="${CONFIGPATH}/${CONFIGFILE}"

if [ ! -f "${CONFIG}" ];
then
  echo "cp sample ini file to ${CONFIG}"
  cp wiseService.ini.sample "${CONFIG}"
fi

exec node wiseService.js -c "${CONFIG}" --workers "${WORKERS}"
